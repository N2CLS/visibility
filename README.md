# Compute visibility of the N2CLS fields

## GILDAS/ASTRO

Change the date run in N2CLS.astro
run

```bash
astro @N2CLS.astro
```


## Astropy/astroplan

Change the date in run in N2CLS.py
run
```bash
python N2CLS.py
```
