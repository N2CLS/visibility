import astroplan
from astropy.table import Table
from astropy.coordinates import SkyCoord, EarthLocation
from astropy.time import Time
from astroplan import FixedTarget, Observer
from astroplan.plots import plot_airmass
import matplotlib.pyplot as plt

plt.ion()

time = Time('2018-05-15 00:00:00')

sources = Table.read('N2CLS.sou', format='ascii.no_header', names=['name', 'sys', 'equinox', 'RA', 'DEC'])
sources['SkyCoord'] =  SkyCoord(sources['RA'], sources['DEC'], unit=("hourangle", "deg"))

targets = []
for source in sources:
    targets.append(FixedTarget(source['SkyCoord'], source['name']))


pico = Observer(EarthLocation.from_geodetic('-03:23:55.51', '37:04:06.29', 2850.0))

fig, ax = plt.subplots()
plot_airmass(targets, pico, time, ax=ax, brightness_shading=True, altitude_yaxis=True)
ax.legend()

fig.tight_layout()
fig.savefig('astroplan.png')
